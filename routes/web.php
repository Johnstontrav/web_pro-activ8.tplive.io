<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');

Route::group([
    'middleware' => ['auth']
], function () {

    Route::resource('/questions', 'Reviewquestions');
    Route::resource('/reviewtypes', 'Reviewtypes');
    Route::resource('/locationtypes', 'Locationtypes');
    Route::resource('/questiongroups', 'Questiongroups');
    Route::resource('/locations', 'Locations');


    Route::get('/locations/image/delete/{id}','Locations@removeImage');
});