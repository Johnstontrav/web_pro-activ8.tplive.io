<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::group([
    'middleware' => ['cors']
], function () {


    Route::post('/register', function (Request $request) {

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ];

        $user = \App\User::create($data);

        $token = $user->createToken();

        $user->token = $token->token;
        $user->expires_at = Carbon::now()->addYears(10)->timestamp;

        return ['profile' => $user];
    });

    Route::get('/login', function (Request $request) {


        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {

            $token = \App\Token::where('user_id', Auth::id())->where('name', 'mobile-app')->first();

            if (!$token) {
                $token = Auth::user()->createToken();
            }

            $user = Auth::user();
            $user->token = $token->token;
            $user->expires_at = Carbon::now()->addYears(10)->timestamp;

            return ['profile' => $user];
        }

        return ['error' => 'no access'];
    });

    Route::resource('test', 'TestController');

    Route::post('upload', function () {

        $path = request()->file('file')->store('locationimages', 's3');

        $img = new \App\Locationimage();

        $img->location_id = 1;
        $img->img = $path;

        $img->save();

        return ["success" => true];

    });

    Route::group([
        'middleware' => ['auth:api']
    ], function () {

        Route::get('/ping', function () {
            return ['hello' => date('Y-m-d H:i:s')];
        });


        Route::post('submit/questions', function (Request $request) {

            $count = \App\Locationreview::where('user_id', Auth::id())->where('location_id', $request->input('location_id'))->where('reviewtype_id', $request->input('reviewtype_id'))->count();

            if ($count > 0) {
                \App\Locationreview::where('user_id', Auth::id())->where('location_id', $request->input('location_id'))->where('reviewtype_id', $request->input('reviewtype_id'))->delete();
            } else {
                \App\Location::find($request->input('location_id'))->increment('review_count');
            }


            foreach ($request->input('data') as $item) {

                $review = new \App\Locationreview();
                $review->location_id = $request->input('location_id');
                $review->reviewtype_id = $request->input('reviewtype_id');
                $review->user_id = Auth::id();
                $review->reviewquestion_id = $item['id'];
                $review->answer = (isset($item['answer']) ? $item['answer'] : 0);

                $review->save();

            }

            return ["success" => true];

        });

        Route::get('/locationtypes', function (Request $request) {
            return \App\Locationtype::get();
        });

        Route::get('/questions/{locationtype}/{reviewtype}', function ($locationtype, $reviewtype, Request $request) {

            $data = \App\Reviewquestion::where('locationtype_id', $locationtype)->where('reviewtype_id', $reviewtype)->get();

            foreach ($data as $k => $v) {
                $data[$k]->answer = true;
            }

            return $data;

        });
        Route::get('/locations/{locationtype_id}', function ($locationtype_id, Request $request) {

            $data = \App\Location::with('images')->with('score')->where('locationtype_id', $locationtype_id)->get();

            foreach ($data as $k => $item) {
                if (isset($item['images'][0]))
                    $data[$k]->img = $item['images'][0]->img;
            }

            return $data;


        });
        Route::get('/location/{id}', function ($id, Request $request) {
            $item = \App\Location::with('score')->with('reviews')->with('images')->find($id);
            if (isset($item['images'][0]))
                $item->img = $item['images'][0]->img;
            return $item;
//            $data->setRelation("reviews",$data->reviews->keyBy('reviewtype_id'));
//            return $data;
        });

        Route::get('/like/{id}', function ($id, Request $request) {

            return \App\Location::find($id)->increment('likes');

        });

    });

});


