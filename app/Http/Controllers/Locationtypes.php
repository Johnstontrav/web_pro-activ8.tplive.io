<?php

namespace App\Http\Controllers;

use App\Locationtype;
use Illuminate\Http\Request;

class Locationtypes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $page['data'] = Locationtype::get();

        return view('locationtypes.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('locationtypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Locationtype();
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "New type added, congrats!");

        return redirect()->route('locationtypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $rec = Locationtype::find($id);

        return view('locationtypes.edit',compact('rec'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Locationtype::find($id);
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "Type updated, congrats!");

        return redirect()->route('locationtypes.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Locationtype::find($id)->delete();

        flash()->success('Success!', "Type removed!");

        return redirect()->route('locationtypes.index');
    }
}
