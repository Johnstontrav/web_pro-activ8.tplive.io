<?php

namespace App\Http\Controllers;

use App\Reviewtype;
use Illuminate\Http\Request;

class Reviewtypes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $page['data'] = Reviewtype::get();

        return view('reviewtypes.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reviewtypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Reviewtype();
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "New type added, congrats!");

        return redirect()->route('reviewtypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $rec = Reviewtype::find($id);

        return view('reviewtypes.edit',compact('rec'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Reviewquestion $reviewquestion)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Reviewtype::find($id);
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "Type updated, congrats!");

        return redirect()->route('reviewtypes.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reviewtype::find($id)->delete();

        flash()->success('Success!', "Type removed!");

        return redirect()->route('reviewtypes.index');
    }
}
