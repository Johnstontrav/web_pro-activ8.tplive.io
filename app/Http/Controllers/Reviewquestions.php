<?php

namespace App\Http\Controllers;

use App\Locationtype;
use App\Questiongroup;
use App\Reviewquestion;
use App\Reviewtype;
use Illuminate\Http\Request;

class Reviewquestions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page['reviewtypes'] = Reviewtype::get();
        $page['locationtypes'] = Locationtype::get();
        $page['questiongroups'] = Questiongroup::get();
        $page['questions'] = Reviewquestion::with('reviewtype')->with('locationtype')->with('questiongroup');


        if ($request->has('reviewtype_id') && !empty($request->get('reviewtype_id')))
            $page['questions'] = $page['questions']->where('reviewtype_id',$request->get('reviewtype_id'))   ;

        if ($request->has('locationtype_id') && !empty($request->get('locationtype_id')))
            $page['questions'] = $page['questions']->where('locationtype_id',$request->get('locationtype_id'))   ;

        if ($request->has('questiongroup_id') && !empty($request->get('questiongroup_id')))
            $page['questions'] = $page['questions']->where('questiongroup_id',$request->get('questiongroup_id'))   ;

        if ($request->has('question') && !empty($request->get('question')))
            $page['questions'] = $page['questions']->where('question','like','%'.$request->get('question').'%')   ;


        if ($request->has('weighting') && !empty($request->get('weighting')))
            $page['weighting'] = $page['questions']->where('weighting',$request->get('weighting'))   ;



        $page['questions'] = $page['questions']->get();

        return view('questions.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['reviewtypes'] = Reviewtype::get();
        $page['locationtypes'] = Locationtype::get();
        $page['questiongroups'] = Questiongroup::get();

        return view('questions.create',compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Reviewquestion();
        $item->reviewtype_id = $request->input('reviewtype_id');
        $item->locationtype_id = $request->input('locationtype_id');
        $item->questiongroup_id = $request->input('questiongroup_id');
        $item->questionorder = $request->input('questionorder');
        $item->weighting = $request->input('weighting');
        $item->question = $request->input('question');

        $item->save();

        flash()->success('Success!', "New question added, congrats!");

        return redirect()->route('questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        $page['reviewtypes'] = Reviewtype::get();
        $page['locationtypes'] = Locationtype::get();
        $page['questiongroups'] = Questiongroup::get();
        $rec = Reviewquestion::find($id);

        return view('questions.edit',compact('page','rec'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Reviewquestion $reviewquestion)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Reviewquestion::find($id);
        $item->reviewtype_id = $request->input('reviewtype_id');
        $item->locationtype_id = $request->input('locationtype_id');
        $item->questiongroup_id = $request->input('questiongroup_id');
        $item->questionorder = $request->input('questionorder');
        $item->question = $request->input('question');
        $item->weighting = $request->input('weighting');

        $item->save();

        flash()->success('Success!', "Question updated, congrats!");

        return redirect()->route('questions.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reviewquestion::find($id)->delete();

        flash()->success('Success!', "Question removed!");

        return redirect()->route('questions.index');
    }
}
