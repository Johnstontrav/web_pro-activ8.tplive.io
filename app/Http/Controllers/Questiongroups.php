<?php

namespace App\Http\Controllers;

use App\Questiongroup;
use Illuminate\Http\Request;

class Questiongroups extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $page['data'] = Questiongroup::get();

        return view('questiongroups.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questiongroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Questiongroup();
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "New group added, congrats!");

        return redirect()->route('questiongroups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $rec = Questiongroup::find($id);

        return view('questiongroups.edit',compact('rec'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Reviewquestion $reviewquestion)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Questiongroup::find($id);
        $item->name = $request->input('name');

        $item->save();

        flash()->success('Success!', "Group updated, congrats!");

        return redirect()->route('questiongroups.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviewquestion  $reviewquestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Questiongroup::find($id)->delete();

        flash()->success('Success!', "Group removed!");

        return redirect()->route('questiongroups.index');
    }
}
