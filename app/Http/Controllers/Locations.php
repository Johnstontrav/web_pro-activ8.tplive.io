<?php

namespace App\Http\Controllers;

use App\Location;
use App\Locationimage;
use App\Locationtype;
use Mapper;
use Illuminate\Http\Request;

class Locations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $page['locationtypes'] = Locationtype::get();
        $page['data'] = Location::with('images')->with('score');


        if ($request->has('locationtype_id') && !empty($request->get('locationtype_id')))
            $page['data'] = $page['data']->where('locationtype_id', $request->get('locationtype_id'));


        if ($request->has('name') && !empty($request->get('name')))
            $page['data'] = $page['data']->where('name', 'like', '%' . $request->get('name') . '%');


        $page['data'] = $page['data']->get();

        return view('locations.index', compact('page'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['locationtypes'] = Locationtype::get();

        return view('locations.create', compact( 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Location();
        $item->locationtype_id = $request->input('locationtype_id');
        $item->name = $request->input('name');
        $item->address = $request->input('address');
        $item->summary = $request->input('summary');
        $item->lat = $request->input('lat');
        $item->lng = $request->input('lng');

        $item->save();

        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $path = $image->store('locationimages', 's3');

                $img = new Locationimage();

                $img->location_id = $item->id;
                $img->img = $path;

                $img->save();
            }
        }

        flash()->success('Success!', "Location updated, congrats!");

        return redirect()->route('locations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page['locationtypes'] = Locationtype::get();
        $rec = Location::with('images')->with('score')->find($id);

        Mapper::map($rec->lat, $rec->lng, ['zoom' => 15]);

        return view('locations.edit', compact('rec', 'page', 'map'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $path = $image->store('locationimages', 's3');

                $img = new Locationimage();

                $img->location_id = $id;
                $img->img = $path;

                $img->save();
            }
        }

        $item = Location::find($id);
        $item->locationtype_id = $request->input('locationtype_id');
        $item->name = $request->input('name');
        $item->address = $request->input('address');
        $item->summary = $request->input('summary');

        $item->save();

        flash()->success('Success!', "Location updated, congrats!");

        return redirect()->route('locations.index');
    }

    public function removeImage($id)
    {

        Locationimage::find($id)->delete();

        flash()->success('Success!', "Image removed");

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::find($id)->delete();

        //todo: remove images

        flash()->success('Success!', "Location removed!");

        return redirect()->route('locations.index');
    }
}
