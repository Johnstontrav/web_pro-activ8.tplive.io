<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{



    public function images()
    {
        return $this->hasMany('App\Locationimage');
    }

    public function reviews()
    {
        return $this->hasMany('App\Reviewscoreslocation');
    }

    public function score()
    {
        return $this->hasOne('App\Reviewtoplevellocation');
    }

}
