<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locationtype extends Model
{


    public function getImgAttribute($value)
    {
        return 'http://pro-activ8.com.au.s3-website-ap-southeast-2.amazonaws.com/'.$value;
    }

}
