<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locationreview extends Model
{

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id','email','name','photo_url']);
    }

}
