<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locationimage extends Model
{


    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function getImgAttribute($value)
    {
        return 'http://pro-activ8.com.au.s3-website-ap-southeast-2.amazonaws.com/'.$value;
    }

}
