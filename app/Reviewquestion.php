<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewquestion extends Model
{

    public function reviewtype()
    {
        return $this->belongsTo('App\Reviewtype');
    }

    public function locationtype()
    {
        return $this->belongsTo('App\Locationtype');
    }

    public function questiongroup()
    {
        return $this->belongsTo('App\Questiongroup');
    }

}
