<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewtoplevellocation extends Model
{

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

}
