<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    {{--<script type="text/javascript"--}}
            {{--src="//maps.googleapis.com/maps/api/js?key=AIzaSyBDGvzp_rECe64XaLgr69o9DloE7r3Ie-g&v=3.exp&sensor=‌​false&libraries=plac‌​es"></script>--}}

    <!-- Scripts -->
    @yield('scripts', '')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>
</head>
<body class="with-navbar">
    <div id="spark-app" v-cloak>
        <!-- Navigation -->
        @if (Auth::check())
            @include('spark::nav.user')
        @else
            @include('spark::nav.guest')
        @endif

        <!-- Main Content -->
        @yield('content')

        <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif
    </div>

    <!-- JavaScript -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="/js/sweetalert.min.js"></script>

    @if(session()->has('flash_message'))
        <script type="text/javascript">
		    swal({
			    title: "{!! session('flash_message.title') !!}",
			    text: "{!! session('flash_message.message') !!}",
			    type: "{!! session('flash_message.level') !!}",
                @if(session('flash_message.timer')) timer: "{!! session('flash_message.timer') !!}" @endif
		    });
        </script>
    @endif

    <script>
	    $('form').submit(function (event) {
		    if ($(this).hasClass('submitted')) {
			    event.preventDefault();
		    }
		    else {
			    $(this).find(':submit').html('<i class="fa fa-spinner fa-spin"></i>');
			    $(this).addClass('submitted');
		    }
	    });
    </script>

</body>
</html>
