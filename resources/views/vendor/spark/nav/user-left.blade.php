<!-- Left Side Of Navbar -->
<li>
    <a href="/locations">Locations</a>
</li>
<li>
    <a href="/questions">Questions</a>
</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setup <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/reviewtypes">Review Types</a></li>
        <li><a href="/locationtypes">Location Types</a></li>
        <li><a href="/questiongroups">Question Groups</a></li>
    </ul>
</li>