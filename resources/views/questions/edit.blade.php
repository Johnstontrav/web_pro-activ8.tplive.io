@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Question</div>

                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="/questions/{{$rec->id}}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="col-md-12">Review Type</label>

                                    <div class="col-md-12">
                                        <select name="reviewtype_id" id="reviewtype_id" class="form-control">
                                            @foreach ($page['reviewtypes'] as $item)
                                                <option value="{{$item->id}}" {{($item->id == $rec->reviewtype_id ? 'selected' : '') }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Location Type</label>

                                    <div class="col-md-12">
                                        <select name="locationtype_id" id="locationtype_id" class="form-control">
                                            @foreach ($page['locationtypes'] as $item)
                                                <option value="{{$item->id}}"  {{($item->id == $rec->locationtype_id ? 'selected' : '') }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-12">Location Type</label>

                                    <div class="col-md-12">
                                        <select name="questiongroup_id" id="questiongroup_id" class="form-control">
                                            @foreach ($page['questiongroups'] as $item)
                                                <option value="{{$item->id}}"  {{($item->id == $rec->questiongroup_id ? 'selected' : '') }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-12">Question</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" maxlength="45" name="question"
                                               value="{{ $rec->question }}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-12">Weighting</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="weighting" value="{{ $rec->weighting }}">
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-md-12">Question Order</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" maxlength="45" name="questionorder"
                                               value="{{ $rec->questionorder }}">
                                    </div>
                                </div>


                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <a href="/questions" class="btn btn-secondary">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                    <div class="panel panel-default">
                        <div class="panel-heading">Delete Question</div>

                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="/questions/{{$rec->id}}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}

                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-danger">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>



            </div>
        </div>
    </home>
@endsection
