@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">
                                Questions
                            </h3>

                            <a href="/questions/create" class="btn btn-default pull-right">New</a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">



                            <form class="form-horizontal" role="form" method="GET" action="/questions">
                            <table class="table">
                                <thead>
                                <tr>

                                    <th>Review</th>
                                    <th>Location</th>
                                    <th>Group</th>
                                    <th>Question</th>
                                    <th class="text-right">Weight</th>
                                    <th class="text-right">Order</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                <tr>

                                    <td>
                                        <select name="reviewtype_id" id="reviewtype_id" class="form-control">
                                            <option value="">-- ALL --</option>
                                            @foreach ($page['reviewtypes'] as $item)
                                                <option value="{{$item->id}}" {{(Input::get('reviewtype_id') == $item->id ? 'selected' : '')}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="locationtype_id" id="locationtype_id" class="form-control">
                                            <option value="">-- ALL --</option>
                                            @foreach ($page['locationtypes'] as $item)
                                                <option value="{{$item->id}}" {{(Input::get('locationtype_id') == $item->id ? 'selected' : '')}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="questiongroup_id" id="questiongroup_id" class="form-control">
                                            <option value="">-- ALL --</option>
                                            @foreach ($page['questiongroups'] as $item)
                                                <option value="{{$item->id}}" {{(Input::get('questiongroup_id') == $item->id ? 'selected' : '')}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" maxlength="45" name="question" value="{{Input::get('question')}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="weighting" value="{{Input::get('weighting')}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" maxlength="45" name="questionorder" value="{{Input::get('questionorder')}}">
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary">
                                            Search
                                        </button>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($page['questions'] as $item)
                                    <tr>

                                        <td>{{$item->reviewtype->name}}</td>
                                        <td>{{$item->locationtype->name}}</td>
                                        <td>{{$item->questiongroup->name}}</td>
                                        <td>{{$item->question}}</td>
                                        <td class="text-right">{{$item->weighting}}</td>
                                        <td class="text-right">{{$item->questionorder}}</td>
                                        <td class="text-right">
                                            <a href="/questions/{{$item->id}}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </home>
@endsection
