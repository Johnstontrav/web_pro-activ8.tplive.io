@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">
                                Location Type
                            </h3>

                            <a href="/locationtypes/create" class="btn btn-default pull-right">New</a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">



                            <form class="form-horizontal" role="form" method="GET" action="/locationtypes">
                            <table class="table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($page['data'] as $item)
                                    <tr>

                                        <td>{{$item->name}}</td>
                                        <td class="text-right">
                                            <a href="/locationtypes/{{$item->id}}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </home>
@endsection
