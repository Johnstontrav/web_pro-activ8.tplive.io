@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">New Question Group</div>

                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="/questiongroups">
                                {{ csrf_field() }}



                                <div class="form-group">
                                    <label class="col-md-12">Name</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" maxlength="45" name="name" autofocus>
                                    </div>
                                </div>

                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();" >
                                            Submit
                                        </button>
                                        <a href="/questiongroups" class="btn btn-secondary">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>



            </div>
        </div>
    </home>
@endsection
