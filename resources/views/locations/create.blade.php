@extends('spark::layouts.app')


@section('content')

    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">

            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">New Location Type</div>

                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="/locations">
                            {{ csrf_field() }}

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-12">Location Type</label>

                                    <div class="col-md-12">
                                        <select name="locationtype_id" id="locationtype_id" class="form-control">
                                            @foreach ($page['locationtypes'] as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-12">Name</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" maxlength="45" name="name"
                                               value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Address</label>

                                    <div class="col-md-12">
                                        <input type="text" id="autocomplete" class="form-control" name="address"
                                               value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Summary</label>

                                    <div class="col-md-12">
                                            <textarea type="text" class="form-control" rows="10"
                                                      name="summary"></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="files">Location photos (can attach more than one):</label>
                                    <input type="file" name="images[]" multiple/>
                                </div>


                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary"
                                                onclick="this.disabled=true;this.form.submit();">
                                            Submit
                                        </button>
                                        <a href="/locations" class="btn btn-secondary">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="lng" name="lng">
                            <input type="hidden" id="lat" name="lat">

                        </form>

                    </div>
                </div>

                <div class="panel panel-default">

                    <div class="panel-heading">
                        Map
                    </div>

                    <div class="panel-body">
                        <div id="map" style="height: 500px"></div>
                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection


@section('scripts')

    <script>

	    var placeSearch, autocomplete, map, marker;

	    function initMap(location) {

		    var map = new google.maps.Map(document.getElementById('map'), {
			    zoom: 16,
			    center: location
		    });
		    var marker = new google.maps.Marker({
			    position: location,
			    map: map
		    });
	    }



	    function initAutocomplete() {

		    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
			    {types: ['geocode'], componentRestrictions: {country: "au"}});

		    autocomplete.addListener('place_changed', fillInAddress);

		    var input = document.getElementById('autocomplete');


	    }

	    function fillInAddress() {
		    // Get the place details from the autocomplete object.
		    var place = autocomplete.getPlace();

		    var componentForm = {
			    street_number: 'short_name',
			    route: 'long_name',
			    locality: 'long_name',
			    administrative_area_level_1: 'short_name',
			    country: 'long_name',
			    postal_code: 'short_name'
		    };
		    var address = {};

		    // Get each component of the address from the place details
		    // and fill the corresponding field on the form.
		    for (var i = 0; i < place.address_components.length; i++) {
			    var addressType = place.address_components[i].types[0];
			    if (componentForm[addressType]) {
				    var val = place.address_components[i][componentForm[addressType]];
				    address[addressType] = val;
			    }
		    }
		    console.log(address);

		    $('#StreetAddress').val(address.street_number + ' ' + address.route);
		    $('#Suburb').val(address.locality);
		    $('#State').val(address.administrative_area_level_1);
		    $('#Postcode').val(address.postal_code);
		    $('#place_code').val(place.place_id);
		    $('#lng').val(place.geometry.location.lng());
		    $('#lat').val(place.geometry.location.lat());


		    var newLatLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

		    initMap({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});


	    }


    </script>

    <script type="text/javascript"
            src="//maps.googleapis.com/maps/api/js?key=AIzaSyBDGvzp_rECe64XaLgr69o9DloE7r3Ie-g&libraries=places&callback=initAutocomplete"
            async defer></script>


@endsection