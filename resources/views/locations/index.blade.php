@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">
                                Locations
                            </h3>

                            <a href="/locations/create" class="btn btn-default pull-right">New</a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="GET" action="/locations">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Location Type</th>
                                        <th>Location Name</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="locationtype_id" id="locationtype_id" class="form-control">
                                                <option value="">-- ALL --</option>
                                                @foreach ($page['locationtypes'] as $item)
                                                    <option value="{{$item->id}}" {{(Input::get('locationtype_id') == $item->id ? 'selected' : '')}}>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{Input::get('name')}}">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-primary">
                                                Search
                                            </button>
                                        </td>
                                    </tr>
                                    </thead>
                                </table>
                            </form>

                            @foreach($page['data']->chunk(3) as $chunk)
                                <div class="row list-group">
                                    @foreach($chunk as $item)


                                        <div class="item  col-xs-4 col-lg-4">
                                            <div class="thumbnail">
                                                @if (isset($item->images[0]))
                                                    <img class="group list-group-image" src="{{$item->images[0]->img}}"
                                                         alt=""/>
                                                @else
                                                    <img class="group list-group-image"
                                                         src="http://via.placeholder.com/339x176"
                                                         alt=""/>
                                                @endif
                                                <div class="caption">
                                                    <h4 class="group inner list-group-item-heading">
                                                        <a href="/locations/{{$item->id}}">{{$item->name}}</a>
                                                    </h4>
                                                    <p class="group inner list-group-item-text">
                                                        {{$item->address}}</p>
                                                </div>
                                            </div>
                                        </div>


                                    @endforeach
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </home>
@endsection
