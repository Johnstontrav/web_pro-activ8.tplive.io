@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Review Type</div>

                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="/reviewtypes/{{$rec->id}}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}


                                <div class="form-group">
                                    <label class="col-md-12">Name</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control" maxlength="45" name="name"
                                               value="{{ $rec->name }}">
                                    </div>
                                </div>

                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <a href="/reviewtypes" class="btn btn-secondary">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                    <div class="panel panel-default">
                        <div class="panel-heading">Delete Review Type</div>

                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="/reviewtypes/{{$rec->id}}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}

                                <!-- Login Button -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-danger">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>



            </div>
        </div>
    </home>
@endsection
